import java.util.Random;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Card {
    int cardValue;
    boolean hidden;

    public Card() throws IOException {
        cardValue = getValue();
        hidden = false;
    }

    public static void main(String[] args) {
        // Card card1 = new Card();
    };

    public int getValue() throws IOException {
        Random rand = new Random();

        // Obtain a number between [0 - 9].
        int cardValue = rand.nextInt(9);

        // Add 1 to the result to get a number from the required range
        // (i.e., [1 - 10]).
        cardValue += 1;
        if (cardValue == 1) {
            System.out.println("Vous venez de piocher un as. Entrez sa valeur : soit 1, soit 11. Thank's");
            // Reading data using readLine

            Scanner in = new Scanner(System.in);
            int ace = in.nextInt();
            if (ace == 1) {
                cardValue = 1;
            } else if (ace == 11) {
                cardValue = 11;
            }
        }
        System.out.println(cardValue);
        return cardValue;
    }
}
