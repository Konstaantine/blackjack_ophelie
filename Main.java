import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        int sumDeck = 0;
        Player player1 = new Player();
        player1.drawCard();
        player1.drawCard();

        while (player1.playing == true && sumDeck < 21) {

            System.out.println("Do you want to draw  another card  ? ");
            // Java program to demonstrate BufferedReader
            // Enter data using BufferReader
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(System.in));

            // Reading data using readLine
            String name = reader.readLine();

            // Printing the read line
            if (name.equals("yes")) {
                player1.drawCard();

            } else if (name.equals("no")) {
                player1.playing = false;
            }

            sumDeck = 0;
            for (var card : player1.deck) {
                System.out.println("Vos cartes sont: " + card.cardValue);
                sumDeck = sumDeck + card.cardValue;

            }
            System.out.println("Votre score est de " + sumDeck);
        }
    }
}
